﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timely.Data.Entities;

namespace Timely.Data
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {

        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Session> Sessions { get; set; }
    }
}
