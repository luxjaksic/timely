﻿using System.Collections.Generic;
using Timely.Data.Entities;

namespace Timely.Data
{
    public interface ITimelyRepository
    {
        IEnumerable<Project> GetAllProjects();
        Project GetProjectByProjectId(int projectId);
        IEnumerable<Session> GetAllSessions();
        IEnumerable<Session> GetSessionsByProjectId(int projectId);

        void AddEntity(object entity);
        bool SaveAll();

        void DeleteProjectById(int id);
        void UpdateEntity(object entity);
        Session GetActiveSession();
    }
}