﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timely.Data.Entities;

namespace Timely.Data
{
    public class TimelyRepository : ITimelyRepository
    {
        private readonly Context _context;

        public TimelyRepository(Context context)
        {
            _context = context;
        }

        public void AddEntity(object entity)
        {
            _context.Add(entity);
        }

        public void DeleteProjectById(int id)
        {
            var project = GetProjectByProjectId(id);
            _context.Remove(project);
        }

        public IEnumerable<Project> GetAllProjects()
        {
            return _context.Projects
                .ToList();
        }

        public IEnumerable<Session> GetAllSessions()
        {
            return _context.Sessions.ToList();
        }

        public Project GetProjectByProjectId(int projectId)
        {
            return _context.Projects
                .Where(p => p.Id == projectId)
                .FirstOrDefault();
        }

        public IEnumerable<Session> GetSessionsByProjectId(int projectId)
        {
            return _context.Sessions
                .Where(s => s.Project.Id == projectId)
                .ToList();
        }

        public bool SaveAll()
        {
            return _context.SaveChanges() > 0;
        }

        public void UpdateEntity(object entity)
        {
            _context.Update(entity);
        }

        public Session GetActiveSession()
        {
            return _context.Sessions
                .Where(x => x.EndDate < new DateTime(1971, 1, 1))
                .FirstOrDefault();
        }
    }
}
