import { Component, OnInit, ViewChild } from '@angular/core';
import { ISession, Session } from '../session/session';
import { SessionService } from '../session/session.service';
import { ProjectService } from './project.service';
import { Subscription, interval } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './project.component.html',
})
export class ProjectComponent implements OnInit {

  constructor(private projectService: ProjectService, private sessionService: SessionService) {
  }

  public projects;
  public errorMessage;
  public isActiveSession = false;
  public activeProjectId = 0;
  public activeSession: ISession;

  public timeDifference = 0;
  public hours;
  public minutes;

  private subscription: Subscription;

  ngOnInit(): void {
    this.projectService.getProjects().subscribe({
      next: projects => {
        this.projects = projects;
      },
      error: err => this.errorMessage = err
    });

    this.sessionService.getActiveSession().subscribe({
      next: session => {
        this.activeSession = session;
        if (session != null) {
          this.activeSession.startDate; 
          this.isActiveSession = true;
          this.activeProjectId = session.projectId;
        }
      },
      error: err => this.errorMessage = err
    });

    this.subscription = interval(1000)
      .subscribe(x => { this.timeDifference });
  }

  public startStopSessionClicked(activeProjectId: number) {
    console.log("Session" + this.isActiveSession);
    this.isActiveSession = !this.isActiveSession;
    this.activeProjectId = activeProjectId;

    if (this.isActiveSession 
      && this.activeSession == undefined) {
      let newSession: Session = {
        id: 0,
        startDate: new Date(),
        endDate: new Date(1, 1, 1),
        note: "",
        projectId: activeProjectId,
      }

      this.sessionService.createSession(newSession).subscribe({
        next: session => {
          this.activeSession = session;
        },
        error: err => this.errorMessage = err
      });
    }
    else {
      this.activeSession.endDate = new Date();

      this.sessionService.updateSession(this.activeSession).subscribe({
        next: session => {
          this.activeSession = undefined;
        },
        error: err => this.errorMessage = err
      });

    }
  }

  public getStartStopButtonText(projectId: number) {
    if (this.isActiveSession && this.activeProjectId == projectId) {
      return this.getTimeDifference();
    }
    else {
      return 'Start';
    }
  }

  public isDisabledButton(projectId: number) {
    if (this.isActiveSession
      && this.activeProjectId != projectId) {
      return true;
    }
    return false;
  }

  public getLink(projectId: number) {
    return "/projectedit?type=edit&id=" + projectId;
  }

  public isLinkEnabled(projectId: number) {
    return this.isActiveSession && this.activeProjectId == projectId;
  }

  private getTimeDifference() {
    if (this.activeSession != null || this.activeSession != undefined) {
      let dateNow = new Date();
      let startDate = new Date(this.activeSession.startDate);
      let timeDifference = dateNow.getTime() - startDate.getTime();

      let minutes = Math.floor((timeDifference / (1000 * 60)) % 60);
      let hours = Math.floor((timeDifference / (1000 * 60 * 60)) % 24);
      return hours + ':' + minutes + 'h';
    }
    else {
      return 0;
    }
  }
}
