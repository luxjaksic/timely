import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { IProject } from './project';

@Injectable({
  providedIn: 'root'
})

export class ProjectService {
  private projectUrl = 'https://localhost:44392/api/projects';

  constructor(private http: HttpClient) {

  }

  getProjects(): Observable<IProject[]> {
    return this.http.get<IProject[]>(this.projectUrl).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getProject(id: number): Observable<IProject> {
    var url = this.projectUrl + `/${id}`;
    return this.http.get<IProject>(url).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }
  createProject(project: IProject): Observable<IProject> {
    return this.http.post<IProject>(this.projectUrl, project).pipe(
      tap(data => console.log("Create object " + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  updateProject(updatedProject: IProject): Observable<IProject> {
    return this.http.put<IProject>(this.projectUrl, updatedProject).pipe(
      tap(data => console.log("Create object " + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  deleteProject(projectId: number): Observable<void> {
    var url = this.projectUrl + `/${projectId}`;

    console.log(url);

    return this.http.delete<void>(url);
  }

  private handleError(err: HttpErrorResponse) {
    console.error('Timely: ' + err.message);

    return throwError(err.message);
  }
}
