export interface IProject {
  id: number;
  name: string;
  description: string;
}

export class Project implements IProject {
    id: number;
    name: string;
    description: string;
}
