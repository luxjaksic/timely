export interface ISession {
  id: number;
  startDate: Date;
  endDate: Date;
  note: string;
  projectId: number;
}

export class Session implements ISession {
  id: number;
  startDate: Date;
  endDate: Date;
  note: string;
  projectId: number;
}
