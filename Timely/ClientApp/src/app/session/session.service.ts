import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { ISession } from './session';

@Injectable({
  providedIn: 'root'
})

export class SessionService {
  private projectUrl = 'https://localhost:44392/api/sessions';
  private activeSessionUrl = 'https://localhost:44392/api/activesession';

  constructor(private http: HttpClient) {

  }

  createSession(session: ISession): Observable<ISession> {
    return this.http.post<ISession>(this.projectUrl, session).pipe(
      tap(data => console.log("Create object " + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  updateSession(session: ISession): Observable<ISession> {
    return this.http.put<ISession>(this.projectUrl, session).pipe(
      tap(data => console.log("Create object " + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getActiveSession(): Observable<ISession> {
    return this.http.get<ISession>(this.activeSessionUrl).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
    console.error('Timely: ' + err.message);

    return throwError(err.message);
  }
}
