import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IProject, Project } from '../project/project';
import { ProjectService } from '../project/project.service';

@Component({
  selector: 'app-home',
  templateUrl: './projectedit.component.html',
})

export class ProjectEditComponent {
  name: string;
  description: string;
  project: IProject;
  errorMessage: string;
  projectId: number;
  type: string;

  constructor(
    private projectService: ProjectService,
    private router: Router,
    private route: ActivatedRoute)
  {
  }

  ngOnInit() {
    this.type = this.route.snapshot.queryParamMap.get('type');
    this.projectId = parseInt(this.route.snapshot.queryParamMap.get('id'));

    if (this.type == 'edit') {
      this.projectService.getProject(this.projectId).subscribe({
        next: project => {
          this.project = project;
        },
        error: err => this.errorMessage = err
      });
    }
  }

  saveProject() {
    let newProject: Project = {
      id: 0,
      name: this.name,
      description: this.description
    }

    if (this.project != null) {
      if (this.name != null) {
        this.project.name = this.name;
      }
      if (this.description != null) {
        this.project.description = this.description;
      }
      this.projectService.updateProject(this.project).subscribe({
        next: project => {
          this.router.navigate(['/projects']);
        },
        error: err => this.errorMessage = err
      });
    }
    else {
      this.projectService.createProject(newProject).subscribe({
        next: project => {
          this.project = project;
          this.router.navigate(['/']);
        },
        error: err => this.errorMessage = err
      });
    }

    console.log("I clicked." + this.name + " " + this.description);
  }

  deleteProject(id: number) {
    this.projectService.deleteProject(id).subscribe({
      next: project => {
        this.router.navigate(['/projects']);
      },
      error: err => this.errorMessage = err
    });
  }
}
