﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Timely.Data;

namespace Timely.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class ActiveSessionController : ControllerBase
    {
        private readonly ITimelyRepository repository;
        private readonly ILogger<SessionsController> logger;

        public ActiveSessionController(ITimelyRepository repository, ILogger<SessionsController> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(repository.GetActiveSession());
            }
            catch (Exception ex)
            {
                logger.LogError($"Exception occured while retriving sessions {ex}");
                return BadRequest("Exception occured while retriving sessions");
            }
        }    
    }
}