﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timely.Data;
using Timely.Data.Entities;

namespace Timely.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly ITimelyRepository repository;
        private readonly ILogger<ProjectsController> logger;

        public ProjectsController(ITimelyRepository repository, ILogger<ProjectsController> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(repository.GetAllProjects());
            }
            catch (Exception ex)
            {
                logger.LogError($"Failed to return get projects: {ex}");
                return BadRequest("Failed to return get projects");
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            try
            {
                var project = repository.GetProjectByProjectId(id);

                if (project != null)
                {
                    return Ok(project);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Failed to return get projects: {ex}");
                return BadRequest("Failed to return get projects");
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody]Project project)
        {
            try
            {
                if (project.Name == null)
                {
                    return BadRequest("Project name is required!");
                }

                repository.AddEntity(project);
                if (repository.SaveAll())
                {
                    return Created($"/api/projects/{project.Id}", project);
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Failed to create new project {ex}");
            }

            return BadRequest("Failed to create new project");
        }

        [HttpDelete("{id:int}")]
        public IActionResult DeleteById(int id)
        {
            try
            {
                repository.DeleteProjectById(id);
                if (repository.SaveAll())
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("Failed to delete");
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Failed to delete: {ex}");
                return BadRequest("Failed to delete");
            }
        }


        [HttpPut]
        public IActionResult Update([FromBody]Project project)
        {
            try
            {
                repository.UpdateEntity(project);
                if (repository.SaveAll())
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("Failed to delete");
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Failed to update project: {ex}");
                return BadRequest("Failed to update project");
            }
        }
    }
}
