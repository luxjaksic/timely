﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Timely.Data;
using Timely.Data.Entities;

namespace Timely.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class SessionsController : ControllerBase
    {
        private readonly ITimelyRepository repository;
        private readonly ILogger<SessionsController> logger;

        public SessionsController(ITimelyRepository repository, ILogger<SessionsController> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(repository.GetAllSessions());
            }
            catch (Exception ex)
            {
                logger.LogError($"Exception occured while retriving sessions {ex}");
                return BadRequest("Exception occured while retriving sessions");
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody]Session session)
        {
            try
            {
                if (session.StartDate == null
                    || session.ProjectId == 0)
                {
                    return BadRequest("Start date and project ids are required!");
                }

                repository.AddEntity(session);
                if (repository.SaveAll())
                {
                    return Created($"/api/sessions/{session.Id}", session);
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Failed to create new project {ex}");
            }

            return BadRequest("Failed to create new project");
        }

        [HttpPut]
        public IActionResult Update([FromBody] Session session)
        {
            try
            {
                if (session.StartDate == null
                    || session.ProjectId == 0)
                {
                    return BadRequest("Start date and project ids are required!");
                }

                repository.UpdateEntity(session);
                if (repository.SaveAll())
                {
                    return Created($"/api/sessions/{session.Id}", session);
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Failed to create new project {ex}");
            }

            return BadRequest("Failed to create new project");
        }
    }
}
